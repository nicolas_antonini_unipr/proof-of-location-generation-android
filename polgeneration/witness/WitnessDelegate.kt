package polgeneration.witness

import polgeneration.structures.ProofOfLocation
import polgeneration.structures.ProofOfLocationID

/**
 * WitnessDelegate is an interface that provides updates for the discovery and the management of provers.
 *
 * The WitnessDelegate interface defines the methods that a delegate of a POLWitness object must adopt.
 * The mandatory methods allows to be notified when a proof of location is confirmed or rejected.
 */
interface WitnessDelegate {
    fun success()
    fun failed(reason: String)

    /**
     * Tells the delegate that the witness has failed to provide a proof of location.
     *
     * The witness calls this method after that the method startWitnessing() has been called
     * if the Proof of Location cannot be generated.
     *
     * @param error: The error that caused the prover to stop.
     * @param proofOfLocationIDs: The IDs of the Proof of Location that failed.
     */
    fun witnessDidFailWithError(error: Exception, proofOfLocationIDs: List<ProofOfLocationID>)

    /**
     * Tells the delegate that the witness sent a proof of location.
     *
     * The witness calls this method after that the method startWitnessing() has been called
     * if a Proof of Location has been sent successfully.
     *
     * @param proofOfLocation: The delivered Proof of Location.
     * @param proofOfLocationID: The id of the Proof of Location.
     */
    fun witnessDidSendProofOfLocation(proofOfLocation: ProofOfLocation, proofOfLocationID: ProofOfLocationID)

    /**
     * Tells the delegate that the witness has rejected a proof of location.
     *
     * The witness calls this method after that the method startWitnessing() has been called
     * if a Proof of Location has been rejected.
     *
     * @param proofOfLocationID: The id of the rejected Proof of Location.
     */
    fun witnessDidRejectProofOfLocation(proofOfLocationID: ProofOfLocationID)
}