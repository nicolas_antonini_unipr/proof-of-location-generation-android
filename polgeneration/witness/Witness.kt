package polgeneration.witness

import polgeneration.ble.POLGeneratorBLEService
import polgeneration.cryptography.Cryptography
import polgeneration.messages.*
import polgeneration.structures.Location
import polgeneration.structures.PeerID
import polgeneration.structures.ProofOfLocation
import polgeneration.structures.ProofOfLocationID
import polgeneration.utils.Constants
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import java.security.SecureRandom
import java.util.*
import kotlin.concurrent.timerTask
import kotlin.math.min

/**
 * The class Witness implements the Witness functionalities in the Proof of Location generation protocol.
 *
 * This class allows to search for a prover for some provided Proof of Location IDs. The objects of
 * this class will call methods on his delegate to provide the results.
 */
class Witness() {

    /* State */
    private var state: WitnessState = WitnessState.idle

    /* configuration */
    private var delegate: WitnessDelegate? = null
    private var witnessID: PeerID? = null
    private var context: Context? = null
    private var signingKey: String = ""
    private var proofOfLocationIDs: List<ProofOfLocationID>? = null
    private var currentProofOfLocationID: ProofOfLocationID? = null
    private var location: Location? = null
    private var timestamp: Double = 0.0

    /* Bluetooth */
    private val bluetoothAdapter: BluetoothAdapter by lazy {
        val bluetoothManager = this.context!!.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothManager.adapter
    }
    private val bleScanner by lazy {
        this.bluetoothAdapter.bluetoothLeScanner
    }
    private val scanSettings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
            .build()
    private var scannedPeripherals: MutableList<BluetoothDevice> = arrayListOf()
    private var scannedPeripheralsIndex: Int = 0

    /* BLE Service */
    private var polService: BluetoothGattService? = null
    private var iD_Characteristic: BluetoothGattCharacteristic? = null
    private var dataTx_Characteristic: BluetoothGattCharacteristic? = null
    private var dataRx_Characteristic: BluetoothGattCharacteristic? = null
    private var dBTx_Characteristic: BluetoothGattCharacteristic? = null
    private var dBRx_Characteristic: BluetoothGattCharacteristic? = null

    /* messages */
    private var messageEBuffer : MutableList<Byte> = arrayListOf()
    private var messageEEncryptedSize: Int = 0
    private var messageE: MessageE? = null
    private var stringH: StringH? = null
    private var stringR: StringR? = null
    private var stringC: StringC? = null

    /* Distance bounding */
    private var startTimes: MutableList<Long> = arrayListOf()
    private var endTimes: MutableList<Long> = arrayListOf()
    private var dbIterationIndex: Int = 0

    /**
     * Class constructor.
     *
     * @param delegate: The delegate of the Witness
     * @param witnessID: The id of the Witness
     * @param context: the context of the application.
     */
    constructor(delegate: WitnessDelegate,
                witnessID: PeerID,
                context: Context): this() {
        this.delegate = delegate
        this.witnessID = witnessID
        this.context = context
                }

    /**
     * Sets the key for the signature of data.
     *
     * @param pvtKey: The private key of the Witness, base64 encoded.
     */
    fun setSigningKey(pvtKey: String) {
        this.signingKey = pvtKey
    }

    /**
     * Begins to search for provers that need for a Proof of Location
     *
     * @param proofsOfLocation: The list of Proof of Location IDs that can be served.
     * @param location: The current location of the Witness.
     * @param timestamp: The current timestamp.
     *
     * @return: true if successful, false if busy.
     */
    fun startWitnessing(proofsOfLocation: List<ProofOfLocationID>,
                        location: Location,
                        timestamp: Double) {
        this.proofOfLocationIDs = proofsOfLocation
        this.location = location
        this.timestamp = timestamp
        this.scannedPeripheralsIndex = 0
        this.scannedPeripherals = arrayListOf()

        //Begin scanning for BLE devices
        bleScanner.startScan(null, scanSettings, scanCallback)
        this.state = WitnessState.scanning

        Timer().schedule(timerTask {
            bleScanner.stopScan(scanCallback)
            loopPeripherals()
        }, (Constants.bleScanTimeout * 1000).toLong())
    }

    /* Handles BLE scanning */
    private val scanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            with(result.device) {
                if (!scannedPeripherals.contains(result.device)) {
                    scannedPeripherals.add(result.device)
                }
                val deviceName = result.device.name
            }
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            state = WitnessState.idle
            delegate?.witnessDidFailWithError(Exception("BLE scan failed."), this@Witness.proofOfLocationIDs!!)
        }
    }

    /* Loops through the peripherals to find a suitable Prover */
    private fun loopPeripherals() {
        state = WitnessState.looping
        if(scannedPeripheralsIndex < scannedPeripherals.size) {
            val peripheral = scannedPeripherals[scannedPeripheralsIndex]
            peripheral.connectGatt(context, false, gattCallback, 2)
        } else {
            state = WitnessState.idle
            delegate?.witnessDidFailWithError(Exception("No Prover found."), this@Witness.proofOfLocationIDs!!)
        }
    }

    private fun sendMessageH(gatt: BluetoothGatt?) {
        this.stringH = StringH()
        dataRx_Characteristic!!.value = stringH!!.getByteArray()
        gatt!!.writeCharacteristic(dataRx_Characteristic)
    }

    private fun startDistanceBounding(gatt: BluetoothGatt?) {
        this.state = WitnessState.distanceBounding
        gatt?.setCharacteristicNotification(dBTx_Characteristic, true);
        val uuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
        val descriptor = dBTx_Characteristic!!.getDescriptor(uuid)
        descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        gatt!!.writeDescriptor(descriptor)
    }

    private fun dbIteration(gatt: BluetoothGatt?) {
        val now = System.currentTimeMillis()
        startTimes.add(now)
        dBRx_Characteristic!!.value = byteArrayOf(stringC!!.getByteArray()[dbIterationIndex])
        gatt!!.writeCharacteristic(dBRx_Characteristic)
    }

    private fun sendProofOfLocation(gatt: BluetoothGatt?) {
        val proofOfLocation = ProofOfLocation(stringR!!, stringC!!, stringH!!, messageE!!, witnessID!!, location!!, timestamp)

        val pubKey = Constants.verifierPublicKey
        val pvtKey = this.signingKey

        val signature = Cryptography.sign(pvtKey, proofOfLocation.getByteArray())

        val discardableKey = ByteArray(16)
        SecureRandom.getInstanceStrong().nextBytes(discardableKey)

        val signedData = proofOfLocation.getByteArray() + signature

        val encryptedData = Cryptography.encryptAES(discardableKey, signedData)

        val encryptedKey = Cryptography.encryptRSA(Base64.getDecoder().decode(pubKey), discardableKey)

        var mergedData = byteArrayOf()
        mergedData += encryptedData!!
        mergedData += byteArrayOf(0x2A, 0x2A, 0x2A)
        mergedData += encryptedKey!!
        mergedData += byteArrayOf(0x2A, 0x2A, 0x2A)

        var encryptedProofOfLocationWithLength = byteArrayOf( (mergedData.size / 256).toByte(), (mergedData.size % 256).toByte())
        encryptedProofOfLocationWithLength += mergedData

        Timer().schedule(timerTask {
            val chunkSize = Constants.maxBLEPacketPayloadSize
            val numberOfChunks = encryptedProofOfLocationWithLength.size.toDouble() / chunkSize.toDouble()
            var numberOfChunksRoundedUp = numberOfChunks.toInt()
            if (numberOfChunksRoundedUp.toDouble() < numberOfChunks) {
                numberOfChunksRoundedUp += 1
            }

            for (chunkIndex in 0 until numberOfChunksRoundedUp) {
                val lastIndex = min(encryptedProofOfLocationWithLength.size - 1, chunkIndex * chunkSize + (chunkSize - 1))
                val startIndex = chunkIndex * chunkSize
                val subBytes = encryptedProofOfLocationWithLength.copyOfRange(startIndex, lastIndex + 1)
                Thread.sleep(100)
                dataRx_Characteristic!!.value = subBytes
                gatt!!.writeCharacteristic(dataRx_Characteristic)
            }
            Thread.sleep(100)
            gatt?.disconnect()
            delegate?.witnessDidSendProofOfLocation(proofOfLocation, currentProofOfLocationID!!)
        }, 10)

    }

    /* Handles BLE connections and communications */
    private val gattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            val deviceName = gatt?.device?.name

            if(status == BluetoothGatt.GATT_SUCCESS) {
            } else {
                gatt?.close()
                gatt?.close()
                scannedPeripheralsIndex += 1
                loopPeripherals()
            }

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                gatt?.discoverServices()
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                if (state == WitnessState.looping) {
                    gatt?.close()
                    scannedPeripheralsIndex += 1
                    loopPeripherals()
                } else if (state != WitnessState.finished) {
                    gatt?.close()
                    state = WitnessState.idle
                    delegate?.witnessDidFailWithError(Exception("Device unexpectedly disconnected."), this@Witness.proofOfLocationIDs!!)
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)

            polService = gatt?.getService(POLGeneratorBLEService.POL_BLE_Service_UUID)

            if (polService != null) {

                iD_Characteristic = polService!!.getCharacteristic(POLGeneratorBLEService.POL_BLE_ID_Characteristic_UUID)
                dBRx_Characteristic = polService!!.getCharacteristic(POLGeneratorBLEService.POL_BLE_DB_RX_UUID)
                dBTx_Characteristic = polService!!.getCharacteristic(POLGeneratorBLEService.POL_BLE_DB_TX_UUID)
                dataRx_Characteristic = polService!!.getCharacteristic(POLGeneratorBLEService.POL_BLE_Data_RX_UUID)
                dataTx_Characteristic = polService!!.getCharacteristic(POLGeneratorBLEService.POL_BLE_Data_TX_UUID)

                gatt?.readCharacteristic(iD_Characteristic)

            } else {
                gatt?.disconnect()
            }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)

            if (characteristic!!.value != null) {
            }

            if(state == WitnessState.looping) {
                var matched = false
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    for (id in this@Witness.proofOfLocationIDs!!) {
                        if (id.toString() == ProofOfLocationID(characteristic.value).toString()) {
                            matched = true
                            this@Witness.currentProofOfLocationID = ProofOfLocationID(characteristic.value)
                        }
                    }

                } else {
                }

                if (matched) {
                    state = WitnessState.matched

                    //Receive notifications on DATA_TX characteristic
                    gatt?.setCharacteristicNotification(dataTx_Characteristic, true);
                    val uuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")
                    val descriptor = dataTx_Characteristic!!.getDescriptor(uuid)
                    descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                    gatt!!.writeDescriptor(descriptor)

                } else {
                    gatt?.disconnect()
                }
            }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt?, descriptor: BluetoothGattDescriptor?, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (state == WitnessState.matched) {
                } else if (state == WitnessState.distanceBounding) {
                    this@Witness.startTimes = mutableListOf()
                    this@Witness.endTimes = mutableListOf()
                    this@Witness.dbIterationIndex = 0
                    this@Witness.stringR = StringR()
                    this@Witness.stringC = StringC()
                    dbIteration(gatt)
                }
            } else {
                gatt?.disconnect()
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?) {
            super.onCharacteristicChanged(gatt, characteristic)

            if (state == WitnessState.matched) {
                if (this@Witness.messageEEncryptedSize == 0) {
                    this@Witness.messageEEncryptedSize = byteValue(characteristic!!.value[0]) * 256 + byteValue(characteristic!!.value[1])
                    messageEBuffer.addAll(characteristic!!.value.asList().drop(2))
                } else {
                    if (messageEBuffer.size <  this@Witness.messageEEncryptedSize) {
                        messageEBuffer.addAll(characteristic!!.value.asList())
                    }
                }

                if  (messageEBuffer.size >= this@Witness.messageEEncryptedSize) {
                    this@Witness.state = WitnessState.messageEReceived
                    this@Witness.messageE = MessageE(data = messageEBuffer.toByteArray())
                    sendMessageH(gatt)
                }

            } else if (state == WitnessState.distanceBounding) {
                val now = System.currentTimeMillis()
                endTimes.add(now)

                val receivedByte = characteristic!!.value[0]
                stringR!!.append(receivedByte)

                if (stringR!!.getByteArray().size < StringR.size) {
                    dbIterationIndex += 1
                    dbIteration(gatt)
                } else {
                    state = WitnessState.checking
                    checkTiming(gatt)
                }
            }
        }

        private fun checkTiming(gatt: BluetoothGatt?) {
            var failed = 0
            for (i in 0 until StringC.size) {
                val roundTripTime = endTimes[i] - startTimes[i]
                if (roundTripTime > (Constants.bleRoundTripTime + Constants.bleOverheadTime)) {
                    failed += 1
                }
            }
            this@Witness.state = WitnessState.checking
            if (failed > Constants.maxDistanceBoundingLatePackets) {
                this@Witness.state = WitnessState.finished
                val result = ProofOfLocationResult(isPoLValid = false)
                dataRx_Characteristic!!.value = result.getByteArray()
                gatt!!.writeCharacteristic(dataRx_Characteristic)
                Timer().schedule(timerTask {
                    gatt.disconnect()
                }, 100)
                delegate?.witnessDidRejectProofOfLocation(this@Witness.currentProofOfLocationID!!)
            } else {
                this@Witness.state = WitnessState.finished
                val result = ProofOfLocationResult(isPoLValid = true)
                dataRx_Characteristic!!.value = result.getByteArray()
                gatt!!.writeCharacteristic(dataRx_Characteristic)
                this@Witness.sendProofOfLocation(gatt)
            }
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicWrite(gatt, characteristic, status)
            if (state == WitnessState.messageEReceived) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    state = WitnessState.stringHSent
                    startDistanceBounding(gatt)
                }
            }
        }
    }

    private fun byteValue(o: Byte): Int = when {
        (o.toInt() < 0) -> 255 + o.toInt() + 1
        else -> o.toInt()
    }

}