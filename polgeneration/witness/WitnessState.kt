package polgeneration.witness

/**
 * WitnessState represents the possible status of a POLWitness object.
 */
enum class WitnessState {

    /** In this status, the Witness object has been initialized but is waiting to witness for a proof of location */
    idle,

    /** In this status, the witness is scanning for BLE peripherals in order to find provers */
    scanning,

    /** In this status, the witness is looping the BLE peripherals it has found to find a suitable prover */
    looping,

    /** In this status, the witness has found a suitable prover */
    matched,

    /** In this status, the witness has received the message E from the prover */
    messageEReceived,

    /** In this status, the witness has sent the string H */
    stringHSent,

    /** In this status, the witness is performing the Distance Bounding protocol */
    distanceBounding,

    /** In this status, the witness is valuating the result of the Distance Bounding phase */
    checking,

    /** In this status, the witness has valuated the result of the distance bounding protocol */
    finished

}