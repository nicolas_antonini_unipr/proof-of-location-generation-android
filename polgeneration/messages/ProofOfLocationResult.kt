package polgeneration.messages

/**
 * ProofOfLocationResult represents the message that communicates if the proof of location is valid
 * or not.
 */
class ProofOfLocationResult {
    private var bytes: ByteArray

    private var successBytes: ByteArray = byteArrayOf(0x01)
    private var failureBytes: ByteArray = byteArrayOf(0x00)

    /**
     * Class constructor.
     *
     * @param isPoLValid: true if the Proof of Location is valid, else false.
     */
    constructor(isPoLValid: Boolean) {
        if (isPoLValid) {
            this.bytes = successBytes
        } else {
            this.bytes = failureBytes
        }
    }

    /**
     * Class constructor.
     *
     * @param data: The data representing the message.
     */
    constructor(data: ByteArray) {
        this.bytes = data
    }

    /**
     * Returns true if the message tells that the Proof of Location is valid, else false.
     */
    fun isValid(): Boolean {
        var isValid = true
        for (i in this.bytes.indices) {
            if (bytes[i] != successBytes[i]) {
                isValid = false
            }
        }
        return isValid
    }

    /**
     * Returns the data representing the result.
     */
    fun getByteArray(): ByteArray {
        return this.bytes
    }

}