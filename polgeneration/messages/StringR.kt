package polgeneration.messages

import polgeneration.utils.Constants

/**
 * StringR represents the string R in the proof of location generation protocol.
 */
class StringR {
    companion object {
        /**
         * The size of the string, in bytes.
         */
        var size: Int = Constants.stringsSize
    }

    private lateinit var bytes: ByteArray

    /**
     * Class constructor.
     *
     * Initializes an empty string R.
     */
    constructor() {
        this.bytes = byteArrayOf()
    }

    /**
     * Appends a new byte to the string R.
     *
     * @param byte: The byte to be appended.
     */
    fun append(byte: Byte) {
        this.bytes += byte
    }

    /**
     * Returns the data representing the string.
     */
    fun getByteArray(): ByteArray {
        return this.bytes
    }
}