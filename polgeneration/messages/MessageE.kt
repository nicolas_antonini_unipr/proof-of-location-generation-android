package polgeneration.messages

import polgeneration.structures.Location
import polgeneration.structures.PeerID
import polgeneration.utils.Constants

/**
 * MessageE represents the message E in the proof of location generation protocol.
 */
class MessageE {
    companion object {
        /**
         * The size, in bytes, of the message E
         */
        var size: Int = Constants.stringsSize * 2 + Constants.peerIdSize + Location.size
    }

    private lateinit var bytes: ByteArray

    /**
     * Class constructor.
     *
     * @param a: The string A.
     * @param a: The string B.
     * @param id: The ID of the Prover.
     * @param location: The location of the Prover.
     */
    constructor(a: StringA, b: StringB, id: PeerID, location: Location) {
        var bytes = byteArrayOf()
        bytes += a.getByteArray()
        bytes += b.getByteArray()
        bytes += id.getByteArray()
        bytes += location.getByteArray()
        this.bytes = bytes
    }

    /**
     * Class constructor.
     *
     * @param data: the data representing the message.
     */
    constructor(data: ByteArray) {
        this.bytes = data
    }

    /**
     * Returns the data representing the message.
     */
    fun getByteArray(): ByteArray {
        return this.bytes
    }

}