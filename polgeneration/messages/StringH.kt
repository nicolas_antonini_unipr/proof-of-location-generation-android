package polgeneration.messages

import polgeneration.utils.Constants
import kotlin.random.Random

/**
 * StringH represents the string H in the proof of location generation protocol.
 */
class StringH {
    companion object {
        /**
         * The size of the string, in bytes.
         */
        var size: Int = Constants.stringsSize
    }

    private lateinit var bytes: ByteArray

    /**
     * Class constructor.
     *
     * @param data: The data representing the string.
     */
    constructor(data: ByteArray) {
        this.bytes = data
    }

    /**
     * Class constructor.
     *
     * Initializes a random string H.
     */
    constructor() {
        this.bytes = byteArrayOf()
        for (i in 0 until StringA.size) {
            this.bytes += Random.Default.nextBytes(1)
        }
    }

    /**
     * Returns the data representing the string.
     */
    fun getByteArray(): ByteArray {
        return this.bytes
    }
}