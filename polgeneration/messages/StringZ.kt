package polgeneration.messages

import polgeneration.utils.Constants
import kotlin.experimental.xor

/**
 * StringZ represents the string Z in the proof of location generation protocol.
 */
class StringZ {
    companion object {
        /**
         * The size of the string, in bytes.
         */
        var size: Int = Constants.stringsSize
    }

    private lateinit var bytes: ByteArray

    /**
     * Class constructor.
     *
     * Initializes the string Z starting from the strings B and H.
     *
     */
    constructor(b: StringB, h: StringH) {
        this.bytes = byteArrayOf()
        for (i in b.getByteArray().indices) {
            this.bytes += b.getByteArray()[i].xor(h.getByteArray()[i])
        }
    }

    /**
     * Returns the data representing the string.
     */
    fun getByteArray(): ByteArray {
        return this.bytes
    }
}