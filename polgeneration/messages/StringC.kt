package polgeneration.messages

import polgeneration.utils.Constants
import kotlin.random.Random

/**
 * StringC represents the string C in the proof of location generation protocol.
 */
class StringC {
    companion object {
        /**
         * The size of the string, in bytes.
         */
        var size: Int = Constants.stringsSize
    }

    private lateinit var bytes: ByteArray

    /**
     * Class constructor.
     *
     * Initializes a random string C.
     */
    constructor() {
        this.bytes = byteArrayOf()
        for (i in 0 until StringC.size) {
            this.bytes += Random.Default.nextBytes(1)
        }
    }

    /**
     * Returns the data representing the string.
     */
    fun getByteArray(): ByteArray {
        return this.bytes
    }
}