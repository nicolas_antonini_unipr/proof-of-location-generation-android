package polgeneration.messages

import polgeneration.utils.Constants
import kotlin.random.Random

/**
 * StringB represents the string B in the proof of location generation protocol.
 */
class StringB {
    companion object {
        /**
         * The size of the string, in bytes.
         */
        var size: Int = Constants.stringsSize
    }

    private lateinit var bytes: ByteArray

    /**
     * Class constructor.
     *
     * Initializes a random string B.
     */
    constructor() {
        this.bytes = byteArrayOf()
        for (i in 0 until StringB.size) {
            this.bytes += Random.Default.nextBytes(1)
        }
    }

    /**
     * Returns the data representing the string.
     */
    fun getByteArray(): ByteArray {
        return this.bytes
    }
}