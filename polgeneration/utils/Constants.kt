package polgeneration.utils

/**
 * A class that contains the configuration of the Proof of Location generation protocol.
 *
 * The class Constants contains a set of constants on which the Proof of Location Exchange protocol
 * relies.
 */
class Constants {
    companion object {

        /**
         * The size, in bytes, of the strings A, B, C, H and R
         */
        var stringsSize: Int = 8

        /**
         * The size, in bytes, of a peer's ID (must be less of the maxBLEPacketPayloadSize)
         */
        var peerIdSize: Int = 16

        /**
         * The size, in bytes, of a Proof of Location ID
         */
        var proofOfLocationIDSize: Int = 20

        /**
         * The maximum size, in bytes, that a BLE packet payload can carry (20 bytes for Bluetooth version > 4.1)
         */
        var maxBLEPacketPayloadSize: Int = 20

        /**
         * The overhead time, in milliseconds, of the BLE communication technology.
         */
        var bleOverheadTime: Int = 100

        /**
         * The RTT (Round Trip Time) allowed in the communication, in milliseconds.
         */
        var bleRoundTripTime: Int = 30

        /**
         * The maximum number of BLE packets that can arrive late before considering the proof
         * of location invalid.
         */
        var maxDistanceBoundingLatePackets: Int = 2

        /**
         * The minimum RSSI, in bytes, before considering a BLE peripheral unreachable.
         */
        var minBLEPeripheralRSSI: Int = -200

        /**
         * The timeout, in seconds, of the BLE scan that searches for provers.
         */
        var bleScanTimeout: Float = 3.0F

        /**
         * The timeout, in seconds, of an attempt to connect to a BLE peripheral.
         */
        var bleConnectionAttemptTimeout: Float = 3.0F

        /**
         * The public key of the verifier, base64 encoded.
         */
        var verifierPublicKey: String = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJR6cZ6GxnnZW0zQsHkHmE1UQo6uMbSCkoGCh3Cr9o3X+51wE2mBQVT7zDcRorAT8ry1rSP2FrKsOq4bpIHEgRECAwEAAQ=="

    }
}