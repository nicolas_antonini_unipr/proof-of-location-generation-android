package polgeneration.prover

import polgeneration.structures.ProofOfLocation
import polgeneration.structures.ProofOfLocationID

/**
 * ProverDelegate is an interface that provides updates for the discovery and the management of witnesses.
 *
 * The ProverDelegate interface defines the methods that a delegate of a POLProver object must adopt.
 * The mandatory methods allows to be notified when a proof of location is confirmed or rejected.
 */
interface ProverDelegate {

    /**
     * Tells the delegate that the prover failed to retrieve a Proof of Location.
     *
     * The prover calls this method after that the method startProving() has been called if the
     * Proof of Location cannot be retrieved.
     *
     * @param error: The error that caused the prover to stop.
     * @param proofOfLocationID: The ID of the Proof of Location that failed.
     */
    fun proverDidFailWithError(error: Exception, proofOfLocationID: ProofOfLocationID)

    /**
     * Tells the delegate that the prover has received a Proof of Location.
     *
     * The prover calls this method after that the method startProving() has been called if a Proof
     * of Location has been received successfully.
     *
     * @param proofOfLocation: The received proof of location.
     * @param proofOfLocationID: The id of the received Proof of Location.
     */
    fun proverDidReceivePoL(proofOfLocation: ProofOfLocation, proofOfLocationID: ProofOfLocationID)

    /**
     * Tells the delegate that the witness has rejected a proof of location.
     *
     * The prover calls this method after that the method startProving() has been called if a
     * Proof of Location has been rejected by the witness.
     *
     * @param proofOfLocationID: The id of the discarded Proof of Location.
     */
    fun witnessDidDiscardPoL(proofOfLocationID: ProofOfLocationID)
}