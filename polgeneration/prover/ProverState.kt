package polgeneration.prover

/**
 * ProverState represents the possible status of a POLProver object.
 */
enum class ProverState {

    /** In this status, the GATT server is not advertising connectable packets and it's ready to start */
    idle,

    /** In this status, a witness has read the PoL ID and the prover is sending the message E */
    connected,

    /** In this status, the prover is performing the distance bounding protocol */
    distanceBounding,

    /** In this status, the prover is waiting for the result of the distance bounding protocol */
    waitingResults,

    /** In this status, the prover is waiting for the proof of location */
    receiving
}