package polgeneration.prover

import polgeneration.ble.POLGeneratorBLEService
import polgeneration.cryptography.Cryptography
import polgeneration.messages.*
import polgeneration.structures.Location
import polgeneration.structures.PeerID
import polgeneration.structures.ProofOfLocation
import polgeneration.structures.ProofOfLocationID
import polgeneration.utils.Constants
import android.bluetooth.*
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import android.os.ParcelUuid
import java.lang.Exception
import java.security.SecureRandom
import java.util.*
import kotlin.concurrent.timerTask
import kotlin.experimental.and
import kotlin.experimental.or
import kotlin.experimental.xor
import kotlin.math.min


/**
 * The class Prover implements the Prover functionalities in the Proof of Location generation protocol.
 *
 * This class allows to search for witnesses for a provided Proof of Location id. The objects of
 * this class will call methods on his delegate to provide the results.
 */
class Prover {

    /* configuration */
    private var delegate: ProverDelegate? = null
    private var proverID: PeerID? = null
    private var context: Context? = null
    private var signingKey: String = ""

    /* messages */
    private var stringA: StringA? = null
    private var stringB: StringB? = null
    private var stringZ: StringZ? = null
    private var messageE: MessageE? = null
    private var proofOfLocationSize: Int? = null
    private var proofOfLocationBuffer: ByteArray? = null
    private var distanceBoundingIterationIndex: Int = 0
    private var proofOfLocationID: ProofOfLocationID? = null
    private var location: Location? = null

    /* bluetooth */
    private var mBluetoothManager: BluetoothManager? = null
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private var mBluetoothGattServer: BluetoothGattServer? = null
    private var mAdvertiser: BluetoothLeAdvertiser? = null
    private var mAdvData: AdvertiseData? = null
    private var mAdvScanResponse: AdvertiseData? = null
    private var mAdvSettings: AdvertiseSettings? = null

    /* GATT service */
    private var iD_Characteristic: BluetoothGattCharacteristic? = null
    private var dataTx_Characteristic: BluetoothGattCharacteristic? = null
    private var dataRx_Characteristic: BluetoothGattCharacteristic? = null
    private var dBTx_Characteristic: BluetoothGattCharacteristic? = null
    private var dBRx_Characteristic: BluetoothGattCharacteristic? = null

    private var state: ProverState = ProverState.idle

    /**
     * Class constructor.
     *
     * @param delegate: The delegate of the Prover
     * @param proverID: The id of the Prover
     * @param context: the context of the application.
     */
    constructor(delegate: ProverDelegate,
                proverID: PeerID,
                context: Context) {
        this.delegate = delegate
        this.proverID = proverID
        this.context = context
    }

    /**
     * Begins to search Proofs of Location for a given ID.
     *
     * @param proofOfLocationID: The id of the Proof of Location
     * @param location: The location of the Prover
     *
     * @return true if successful, false if failed
     */
    fun startRequesting(proofOfLocationID: ProofOfLocationID, location: Location): Boolean {
        this.proofOfLocationID = proofOfLocationID
        this.location = location

        if (mBluetoothManager == null) {
            mBluetoothManager = this.context!!.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            if (mBluetoothManager == null) {
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager!!.getAdapter();
        if (mBluetoothAdapter == null) {
            return false;
        }

        addDefinedGattServerServices();

        return true
    }

    /**
     * Sets the key for the signature of data.
     *
     * @param pvtKey: The private key of the Prover, base64 encoded.
     */
    fun setSigningKey(pvtKey: String) {
        this.signingKey = pvtKey
    }

    /* configures the GATT server and starts advertising */
    private fun addDefinedGattServerServices() {
        mBluetoothGattServer = mBluetoothManager!!.openGattServer(this.context, mGattServerCallback)
        val polService = BluetoothGattService(POLGeneratorBLEService.POL_BLE_Service_UUID, BluetoothGattService.SERVICE_TYPE_PRIMARY)

        iD_Characteristic = BluetoothGattCharacteristic(POLGeneratorBLEService.POL_BLE_ID_Characteristic_UUID,
                BluetoothGattCharacteristic.PROPERTY_READ,
                BluetoothGattCharacteristic.PERMISSION_READ)

        iD_Characteristic!!.value = proofOfLocationID!!.getByteArray()

        dataTx_Characteristic = BluetoothGattCharacteristic(POLGeneratorBLEService.POL_BLE_Data_TX_UUID,
                BluetoothGattCharacteristic.PROPERTY_READ or BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_READ)

        val dataTx_Descriptor = BluetoothGattDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"),
                BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE)

        dataTx_Descriptor.value = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE

        dataTx_Characteristic!!.addDescriptor(dataTx_Descriptor)

        dataRx_Characteristic = BluetoothGattCharacteristic(POLGeneratorBLEService.POL_BLE_Data_RX_UUID,
                BluetoothGattCharacteristic.PROPERTY_WRITE or BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE,
                BluetoothGattCharacteristic.PERMISSION_WRITE)

        dBTx_Characteristic = BluetoothGattCharacteristic(POLGeneratorBLEService.POL_BLE_DB_TX_UUID,
                BluetoothGattCharacteristic.PROPERTY_NOTIFY,
                BluetoothGattCharacteristic.PERMISSION_READ)

        val dBTx_Descriptor = BluetoothGattDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"),
                BluetoothGattDescriptor.PERMISSION_READ or BluetoothGattDescriptor.PERMISSION_WRITE)

        dBTx_Characteristic!!.addDescriptor(dBTx_Descriptor)

        dBTx_Descriptor.value = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE

        dBRx_Characteristic = BluetoothGattCharacteristic(POLGeneratorBLEService.POL_BLE_DB_RX_UUID,
                BluetoothGattCharacteristic.PROPERTY_WRITE_NO_RESPONSE,
                BluetoothGattCharacteristic.PERMISSION_WRITE)

        polService.addCharacteristic(iD_Characteristic)
        polService.addCharacteristic(dataTx_Characteristic)
        polService.addCharacteristic(dataRx_Characteristic)
        polService.addCharacteristic(dBTx_Characteristic)
        polService.addCharacteristic(dBRx_Characteristic)

        mAdvSettings = AdvertiseSettings.Builder().setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_BALANCED)
                .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_MEDIUM).setConnectable(true).build()

        mAdvData = AdvertiseData.Builder().setIncludeTxPowerLevel(true).addServiceUuid(ParcelUuid(POLGeneratorBLEService.POL_BLE_Service_UUID))
                .build()

        mAdvScanResponse = AdvertiseData.Builder().setIncludeDeviceName(true).build()

        mAdvertiser = mBluetoothAdapter!!.bluetoothLeAdvertiser

        mAdvertiser!!.startAdvertising(mAdvSettings, mAdvData, mAdvCallback)

        mBluetoothGattServer!!.addService(polService)
    }

    private val mGattServerCallback: BluetoothGattServerCallback = object : BluetoothGattServerCallback() {
        override fun onConnectionStateChange(device: BluetoothDevice, status: Int, newState: Int) {
            super.onConnectionStateChange(device, status, newState)
        }

        override fun onServiceAdded(status: Int, service: BluetoothGattService) {
            super.onServiceAdded(status, service)
        }

        override fun onCharacteristicReadRequest(device: BluetoothDevice, requestId: Int, offset: Int, characteristic: BluetoothGattCharacteristic) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic)
            if (state == ProverState.idle) {
                mBluetoothGattServer!!.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, proofOfLocationID!!.getByteArray())
            }

        }

        override fun onCharacteristicWriteRequest(device: BluetoothDevice, requestId: Int, characteristic: BluetoothGattCharacteristic, preparedWrite: Boolean, responseNeeded: Boolean, offset: Int, value: ByteArray) {
            super.onCharacteristicWriteRequest(device, requestId, characteristic, preparedWrite, responseNeeded, offset, value)

            if (state == ProverState.connected) {
                val stringH = StringH(data = value)
                stringZ = StringZ(stringB!!, stringH)
                mBluetoothGattServer!!.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS, offset, value)
            } else if (state == ProverState.distanceBounding) {
                val i = distanceBoundingIterationIndex

                val responseByte = (stringA!!.getByteArray()[i] and (value[0] xor 0xFF.toByte())) or (stringZ!!.getByteArray()[i] and value[0])
                dBTx_Characteristic!!.value = byteArrayOf(responseByte)
                mBluetoothGattServer!!.notifyCharacteristicChanged(device, dBTx_Characteristic!!, false)

                if (i == (StringC.size - 1) ) {
                    state = ProverState.waitingResults
                }

                distanceBoundingIterationIndex += 1
            } else if (state == ProverState.waitingResults) {
                val result = ProofOfLocationResult(data = value)

                if (!result.isValid()) {
                    state = ProverState.idle
                    delegate?.witnessDidDiscardPoL(proofOfLocationID!!)
                } else {
                    state = ProverState.receiving
                    proofOfLocationSize = null
                }
            } else if (state == ProverState.receiving) {
                if (proofOfLocationSize == null) {
                    proofOfLocationSize = byteValue(value[0]) * 256 + byteValue(value[1])
                    proofOfLocationBuffer = value.asList().drop(2).toByteArray()
                } else {
                    proofOfLocationBuffer = proofOfLocationBuffer!! + value
                }
                if (proofOfLocationBuffer!!.size == proofOfLocationSize) {
                    val proofOfLocation = ProofOfLocation(data = proofOfLocationBuffer!!)
                    state = ProverState.idle
                    delegate?.proverDidReceivePoL(proofOfLocation, proofOfLocationID!!)
                }
            }
        }

        override fun onDescriptorReadRequest(device: BluetoothDevice, requestId: Int, offset: Int, descriptor: BluetoothGattDescriptor) {
            super.onDescriptorReadRequest(device, requestId, offset, descriptor)
        }

        override fun onDescriptorWriteRequest(device: BluetoothDevice, requestId: Int, descriptor: BluetoothGattDescriptor, preparedWrite: Boolean, responseNeeded: Boolean, offset: Int, value: ByteArray) {
            super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite, responseNeeded, offset, value)

            if (state == ProverState.idle) {
                state = ProverState.connected
                mBluetoothGattServer!!.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS,offset, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                this@Prover.stringA = StringA()
                this@Prover.stringB= StringB()
                this@Prover.messageE = MessageE(this@Prover.stringA!!, this@Prover.stringB!!, proverID!!, location!!)
                val pubKey = Constants.verifierPublicKey
                val pvtKey = this@Prover.signingKey
                val signature = Cryptography.sign(pvtKey, messageE!!.getByteArray())
                val discardableKey = ByteArray(16)
                SecureRandom.getInstanceStrong().nextBytes(discardableKey)
                val signedData = this@Prover.messageE!!.getByteArray() + signature
                val encryptedData = Cryptography.encryptAES(discardableKey, signedData)
                val encryptedKey = Cryptography.encryptRSA(Base64.getDecoder().decode(pubKey), discardableKey)
                var mergedData = byteArrayOf()
                mergedData += encryptedData!!
                mergedData += byteArrayOf(0x2A, 0x2A, 0x2A)
                mergedData += encryptedKey!!
                mergedData += byteArrayOf(0x2A, 0x2A, 0x2A)
                var encryptedMessageEWithLength = byteArrayOf( (mergedData.size / 256).toByte(), (mergedData.size % 256).toByte())
                encryptedMessageEWithLength += mergedData
                Timer().schedule(timerTask {
                    val chunkSize = Constants.maxBLEPacketPayloadSize
                    val numberOfChunks = encryptedMessageEWithLength.size.toDouble() / chunkSize.toDouble()
                    var numberOfChunksRoundedUp = numberOfChunks.toInt()
                    if (numberOfChunksRoundedUp.toDouble() < numberOfChunks) {
                        numberOfChunksRoundedUp += 1
                    }
                    for (chunkIndex in 0 until numberOfChunksRoundedUp) {
                        val lastIndex = min(encryptedMessageEWithLength.size - 1, chunkIndex * chunkSize + (chunkSize - 1))
                        val startIndex = chunkIndex * chunkSize
                        val subBytes = encryptedMessageEWithLength.copyOfRange(startIndex, lastIndex + 1)
                        Thread.sleep(100)
                        dataTx_Characteristic!!.value = subBytes
                        mBluetoothGattServer!!.notifyCharacteristicChanged(device, dataTx_Characteristic!!, false)
                    }
                    Thread.sleep(100)
                }, 10)
            } else if (state == ProverState.connected) {
                state = ProverState.distanceBounding
                distanceBoundingIterationIndex = 0
                mBluetoothGattServer!!.sendResponse(device, requestId, BluetoothGatt.GATT_SUCCESS,offset, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
            }

        }

        override fun onExecuteWrite(device: BluetoothDevice, requestId: Int, execute: Boolean) {
            super.onExecuteWrite(device, requestId, execute)
        }

        override fun onNotificationSent(device: BluetoothDevice?, status: Int) {
            super.onNotificationSent(device, status)
        }
    }

    private val mAdvCallback: AdvertiseCallback = object : AdvertiseCallback() {

        override fun onStartFailure(errorCode: Int) {
            super.onStartFailure(errorCode)
            when (errorCode) {
                ADVERTISE_FAILED_ALREADY_STARTED -> {
                }
                else -> {
                    delegate?.proverDidFailWithError(Exception("Unable to start BLE advertising"), proofOfLocationID!!)
                }
            }
        }

        override fun onStartSuccess(settingsInEffect: AdvertiseSettings) {
            super.onStartSuccess(settingsInEffect)
        }
    }

    private fun byteValue(o: Byte): Int = when {
        (o.toInt() < 0) -> 255 + o.toInt() + 1
        else -> o.toInt()
    }

}