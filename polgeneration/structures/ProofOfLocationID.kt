package polgeneration.structures

/**
 * ProofOfLocationID is a class that represents the ID of a Proof of Location.
 *
 * The ProofOfLocationID class defines methods to construct and manage Proof of Location IDs.
 */
class ProofOfLocationID {
    var stringID: String = ""

    private lateinit var bytes: ByteArray

    /**
     * Class constructor.
     *
     * @param polId: The string representing the ID of the Proof of Location.
     */
    constructor(polId: String) {
        this.stringID = polId
    }

    /**
     * Class constructor.
     *
     * @param data: The data representing the ID of the Proof of Location.
     */
    constructor(data: ByteArray) {
        this.stringID = String(data)
    }

    /**
     * Returns the data representing the Proof of Location ID.
     */
    fun getByteArray(): ByteArray {
        return this.stringID.toByteArray()
    }

    /**
     * Returns the ID of the Proof of Location.
     */
    override fun toString(): String {
        return stringID
    }
}