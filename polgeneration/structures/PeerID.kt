package polgeneration.structures

/**
 * PeerID represents the pseudonym of a peer in the Proof of Location generation protocol.
 */
class PeerID {
    private var stringID: String = ""

    /**
     * Class constructor.
     *
     * @param peerId: The string representing the ID of the peer.
     */
    constructor(peerId: String) {
        this.stringID = peerId
    }

    /**
     * Class constructor.
     *
     * @param data: The data representing the ID of the peer.
     */
    constructor(data: ByteArray) {
        this.stringID = String(data)
    }

    /**
     * Returns the data representing the peer's ID.
     */
    fun getByteArray(): ByteArray {
        return this.stringID.toByteArray()
    }
}