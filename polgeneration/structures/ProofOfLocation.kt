package polgeneration.structures

import polgeneration.messages.MessageE
import polgeneration.messages.StringC
import polgeneration.messages.StringH
import polgeneration.messages.StringR
import java.nio.ByteBuffer

/**
 * ProofOfLocation represents a proof of location.
 */
class ProofOfLocation {

    private lateinit var bytes: ByteArray

    /**
     * Class constructor.
     *
     * @param r: The string R sent from the prover to the witness
     * @param c: The string R sent from the witness to the prover
     * @param h: The string H
     * @param e: The message E sent from the prover to the witness
     * @param witnessId: The id of the witness
     * @param location: The location of the witness
     * @param timestamp: A timestamp
     */
    constructor(r: StringR, c: StringC, h: StringH, e: MessageE, witnessId: PeerID, location: Location, timestamp: Double) {
        this.bytes = byteArrayOf()
        this.bytes += r.getByteArray()
        this.bytes += c.getByteArray()
        this.bytes += h.getByteArray()
        this.bytes += witnessId.getByteArray()
        this.bytes += location.getByteArray()
        this.bytes += ByteBuffer.allocate(java.lang.Double.BYTES).putDouble(timestamp).array()

        this.bytes += byteArrayOf(0x43, 0x43, 0x43)
        this.bytes += e.getByteArray()
        this.bytes += byteArrayOf(0x43, 0x43, 0x43)
    }

    /**
     * Class constructor.
     *
     * @param data: The data representing the Proof of Location.
     */
    constructor(data: ByteArray) {
        this.bytes = data
    }

    /**
     * Returns the data representing the Proof of Location.
     */
    fun getByteArray(): ByteArray {
        return this.bytes
    }
}