package polgeneration.structures

import java.nio.ByteBuffer

/**
 * Location represents the coordinates of a peer.
 */
class Location {
    companion object {

        /**
         * The size, in bytes, of the data representing the location.
         */
        var size: Int = 16
    }

    /**
     * The latitude of the peer.
     */
    var latitude: Double = 0.0

    /**
     * The longitude of the peer.
     */
    var longitude: Double = 0.0

    /**
     * Class constructor.
     *
     * @param latitude: The latitude of the peer.
     * @param longitude: The longitude of the peer.
     */
    constructor(latitude: Double, longitude: Double) {
        this.latitude = latitude
        this.longitude = longitude
    }

    /**
     * Gets the data representing the location.
     */
    fun getByteArray(): ByteArray {
        var bytes = byteArrayOf()
        bytes += ByteBuffer.allocate(java.lang.Double.BYTES).putDouble(this.latitude).array()
        bytes += ByteBuffer.allocate(java.lang.Double.BYTES).putDouble(this.longitude).array()
        return bytes
    }
}