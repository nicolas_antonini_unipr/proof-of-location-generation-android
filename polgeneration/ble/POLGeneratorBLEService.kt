package polgeneration.ble

import java.util.*

/**
 * A class that defines the Bluetooth Low Energy GATT Service of the Proof Of Location Exchange protocol.
 *
 * The DSGBLEService class defines the UUIDs of the GATT service and characteristics of the Proof Of Location Exchange protocol.
 * These characteristics will be exposed by the prover (the GATT server) when searching for a verifier.
 */
abstract class POLGeneratorBLEService {
    companion object {

        /** The UUID of the GATT Service that contains the Proof of Location Exchange characteristics */
        var POL_BLE_Service_UUID: UUID = UUID.fromString("ead001a1-c9bc-4a6e-a787-8e26621b7403")

        /** The UUID of the GATT Characteristic that exposes the proof of location id to the witness */
        var POL_BLE_ID_Characteristic_UUID: UUID = UUID.fromString("4bc06035-4a36-43df-af76-9c057633453e")

        /** The UUID of the GATT Characteristic used to send data for a distance bounding iteration from a prover to a witness */
        var POL_BLE_DB_TX_UUID: UUID = UUID.fromString("58d07345-411f-4a94-b8f3-845c9d6ffeab")

        /** The UUID of the GATT Characteristic used to send data for a distance bounding iteration from a witness to a prover */
        var POL_BLE_DB_RX_UUID: UUID = UUID.fromString("05ca3930-de60-469d-85d3-6be7826b0fc4")

        /** The UUID of the GATT Characteristic used to send data from the prover to the witness */
        var POL_BLE_Data_TX_UUID: UUID = UUID.fromString("4a59c0dc-7176-415c-a7ba-5e54f769650c")

        /** The UUID of the GATT Characteristic used to send data from the witness to the prover */
        var POL_BLE_Data_RX_UUID: UUID = UUID.fromString("323546e5-95b6-4fb6-8efa-08b40fcd2869")

    }
}