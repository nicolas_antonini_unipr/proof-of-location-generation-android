package polgeneration.cryptography

import java.security.KeyFactory
import java.security.PrivateKey
import java.security.Signature
import java.security.spec.EncodedKeySpec
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * The object Cryptography handles data encryption.
 */
object Cryptography {

    /**
     * Encrypts data with EAS180 CBC and PKCS5 padding, with an empty IV.
     *
     * @param key: The key.
     * @param value: The data to be encrypted.
     */
    fun encryptAES(key: ByteArray, value: ByteArray): ByteArray? {
        try {
            val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
            val iv = IvParameterSpec(byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00))

            val skeySpec = SecretKeySpec(key, "AES")
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv)

            return cipher.doFinal(value)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        return null
    }

    /**
     * Encrypts data with RSA.
     *
     * @param key: The public key.
     * @param value: The data to be encrypted.
     */
    fun encryptRSA(key: ByteArray, value: ByteArray): ByteArray? {
        val keySpec: EncodedKeySpec = X509EncodedKeySpec(key)
        val keyFactory = KeyFactory.getInstance("RSA")
        val key = keyFactory.generatePublic(keySpec)
        val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
        cipher.init(Cipher.ENCRYPT_MODE, key)
        return cipher.doFinal(value)
    }

    /**
     * Creates a digital signature of data.
     *
     * @param key: The private key.
     * @param value: The data to be signed.
     */
    fun sign(key: String, value: ByteArray): ByteArray {
        val pk = loadPrivateKey(key)
        val sig: Signature = Signature.getInstance("SHA256withRSA")
        sig.initSign(pk)
        sig.update(value)
        return sig.sign()
    }

    private fun loadPrivateKey(key64: String): PrivateKey? {
        val clear: ByteArray = Base64.getDecoder().decode(key64)
        val keySpec = PKCS8EncodedKeySpec(clear)
        val fact = KeyFactory.getInstance("RSA")
        val priv = fact.generatePrivate(keySpec)
        Arrays.fill(clear, 0.toByte())
        return priv
    }

}