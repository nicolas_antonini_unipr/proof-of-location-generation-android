# PoL Generation

The PoL Generation library implements the Prover and Witness functionalities of the Proof of Location Generation protocol. 

## Requirements

The library requires Android API Level 18 in order to run. 

The following permission are required in order to use both the Prover and Witness functionalities:

- android.permission.ACCESS_FINE_LOCATION

- android.permission.BLUETOOTH_ADMIN

- android.permission.BLUETOOTH

- android.permission.BLUETOOTH_ADVERTISE

## Usage - Prover
The following code allows to start a new Prover and receive Proofs of Location for a given Proof of Location ID.

`val proverID = PeerID(peerId = "ExampleProversId")`

`val polId = ProofOfLocationID(polId = "ProofOfLocationID123")`

`val location = Location(0.0,0.0)`

`val prover = Prover(this, proverID, requireContext())`

`prover.setSigningKey(privateKey)`

`val success = prover.startRequesting(polId, location)`

In the previous snippet, the peer:

- creates a `PeerID` representing its idendity.

- creates a `ProofOfLocationID` representing the ID of the Proof of Location he wants to collect.

- creates a `Location`, representing its current coordinates.

- creates the `Prover` instance that will collect the Proofs of Location. The Prover requires a delegate that will receive updates about events occurring during the execution of the protocol.

- sets its RSA private signing key (512 bit), base64 encoded.

- starts the Prover, that will receive Proof of Locations from the nearby witnesses. The variable `success` will be true if the prover started successfully.

The delegate of the `Prover` must implement the `ProverDelegate` interface.


## Usage - Witness

The following code allows to start a new Witness and provide Proofs of Location for a given list of Proof of Location IDs.

`val witnessID = PeerID(peerId = "ExampleWitnessId")`

`val polId1 = ProofOfLocationID(polId = "ProofOfLocationID123")`

`val polId2 = ProofOfLocationID(polId = "ProofOfLocationID456")`

`val location = Location(0.0,0.0)`

`val witness = Witness(this, witnessID, requireContext())`

`witness.setSigningKey(privateKey)`

`witness.startWitnessing(listOf(polId1, polId2), location, timestamp)`


In the previous snippet, the peer:

- creates a `PeerID` representing its idendity.

- creates two `ProofOfLocationID` representing the IDs of the Proofs of Location he can provide.

- creates a `Location`, representing its current coordinates.

- creates the `Witness` instance that provide the Proofs of Location to the Provers. The Witness requires a delegate that will receive updates about events occurring during the execution of the protocol.

- sets its RSA private signing key (512 bit), base64 encoded.

- starts the Witness, that will provide Proofs of Locations to the nearby Provers. 


The delegate of the `Witness` must implement the `WitnessDelegate` interface.